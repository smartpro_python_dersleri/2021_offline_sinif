from dataclasses import dataclass


@dataclass
class Hayvan:
    name: str
    species: str
    gender: str
    age: int = 1

    def __str__(self):
        return f'{self.species} - {self.name}'
    
    def add_age(self):
        self.age += 1


bozo = Hayvan(
    name='Bozo',
    species='dog',
    gender='Female'
)

print(bozo.age)
bozo.add_age()
print(bozo.age)