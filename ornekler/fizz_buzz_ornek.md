* 1 ile 50 arası bir döngü dönecek
* döngümüzde eğer sayı 3 ile bölünüyorsa ekrana "Fizz" yazdırılacak
* Eger sayı 5 ile bölünüyorsa ekrana "Buzz" yazdırılacak
* Eğer hem 3 hem de 5 ile bölünüyorsa ekrana "FizzBuzz" yazdırılacak
* Diğer bütün senaryolarda direkt sayı yazdırılacak