ornek_metin = "Sen Abdülhamidi savunmuşsun! Hayır savunmadım! Savundun! Çıkar Göster!"

def say_beni_abi(metin, filtreler='aeıioöuü!', yazdir=True):
    metin = metin.lower()
    cikti = {}
    for i in filtreler:
        cikti.update({i: metin.count(i)})

    if yazdir:
        for k, v in cikti.items():
            print(k, "=", v)
    return cikti

#say_beni_abi(ornek_metin)
#say_beni_abi(ornek_metin, filtreler='aeıioöuü!', yazdir=True)

#say_beni_abi('Bana bir cümle söyle.', filtreler='ba')