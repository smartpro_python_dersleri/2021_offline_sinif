* Öğrenci bilgileri tutan bir uygulama yapılacak
* Bu uygulamada öğrencilerin aşağıdaki bilgileri tutulacak
* "Ad", "Soyad", "Sınıf", "Yaş", "Cinsiyet", "Öğrenci No", "not ortalaması"
* uygulama çalıştığında öğrenciler listelenecek ve öğrenci seçme ekranına gelecek
* Bu uygulamada sırasını ya da numarasını belirttiğimiz öğrencinin istediğimiz bilgisi düzenlenebilir hale gelecek
* Bir düzenleme yapıldıktan sonra tekrar öğrenci seçmeye gelecek
* Öğrenci seçme ekranında "Çıkış" ya da "Exit" yazıldığı taktirde program kapanacak

* Programın içeriğini bir dosyaya kaydetmek isteğe bağlıdır

```python
ornek = {
    '449': {
        'name': 'Ahmet'
    }
}

ornek['449']['name']
```