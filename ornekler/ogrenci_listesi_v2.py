import json
from dataclasses import dataclass


@dataclass
class Student:
    id: str
    name: str
    surname: str
    age: int
    gender: str
    classroom: str
    gpa: int = 0

    # dataclass yukarıda girdiğimiz özellikleri kullanarak bize aşağıdaki gibi bir init oluşturuyor
    """
    def __init__(self, **kwargs):
        self.id = kwargs['id']
        self.name = kwargs['name']
        self.surname = kwargs['surname']
        self.age = kwargs['age']
        self.gender = kwargs['gender']
        self.classroom = kwargs['classroom']
        self.gpa = kwargs.get('gpa')
    """

    def __str__(self):
        return f'{self.id} - {self.name} {self.surname}'
    
    @staticmethod
    def get_fields():
        return ('name', 'surname', 'age', 'gender', 'classroom', 'gpa')

    def print_student(self):
        print(*self.__dict__.values(), sep=' | ')

    def get_field_name(self):
        while True:
            print('Available Fields:', *Student.get_fields())
            field = str(input('Please select a field to modify: '))
            if field.lower() in Student.get_fields():
                return field.lower()
            print('Please select a correct field!')
    
    def edit_student(self):
        while True:
            field = self.get_field_name()
            value = input(f'Please provide a value for "{field}": ')
            setattr(self, field, value)
            if input('If you want to quit, plase write "q"').lower() == 'q':
                break

    @classmethod
    def create_new_student(cls, id):
        student_args = {'id': id}
        for i in cls.get_fields():
            value = input(f'Please provide {i}: ')
            student_args.update({i: value})
        
        return cls(**student_args)
        # Burada **student_args şeklinde yazmamızın tek sebebi, aşağıdaki gibi bir
        # angaryadan kurtulmak. ** burada verdiğimiz dict içeriklerini birer argümana çeviriyor
        """
        return cls(
            name=student_args['name'],
            surname=student_manager['surname'],
            age=student_args['age']
            .....)
        """
    

class StudentManager:
    student_dict = {}
    file_name = 'student_list_v2.json'

    def __init__(self):
        self.__read_from_file__()

    def check_student_exists(self, id):
        return id in self.student_dict

    def get_students(self):
        output_dict = {}
        for k, v in self.student_dict.items():
            output_dict.update({k: v.__dict__})
        return output_dict
    
    def print_students(self):
        print('id', *Student.get_fields(), sep=' | ')
        for i in self.student_dict.values():
            i.print_student()

    def get_student_id(self):
        while True:
            value = str(input('Please provide a new student ID: '))
            if not self.check_student_exists(value):
                return value
            print(f'{value} is already exists in the database!')

    def add_student(self, override_file=None):
        id = self.get_student_id()
        student = Student.create_new_student(id)
        
        self.student_dict.update({id: student})
        self.write_to_file(override_file=override_file)

    def edit_students(self, id, override_file=None):
        student = self.student_dict[id]
        student.edit_student()

        self.student_dict.update({student.id: student})
        self.write_to_file(override_file=override_file)
    
    def write_to_file(self, override_file=None):
        if override_file:
            file_name = override_file
        else:
            file_name = self.file_name
        with open(file_name, 'w') as f:
            f.write(json.dumps(self.get_students()))
    
    def __read_from_file__(self, override_file=None):
        if override_file:
            file_name = override_file
        else:
            file_name = self.file_name

        raw_student_dict = None
        try:
            with open(file_name) as f:
                raw_student_dict = json.loads(f.read())
        except FileNotFoundError:
            raw_student_dict = {}
        except ValueError as e:
            print('Dosya formati hatali')
            print(type(e))
            exit()
    
        for key, value in raw_student_dict.items():
            self.student_dict.update({
                key: Student(**value)
            })


def main():
    student_manager = StudentManager()

    while True:
        print("""
    Lütfen yapacağınız işlemi seçiniz.
    Öğrenci no: Bir öğrenciyi düzenle
    New: Yeni bir öğrenci oluştur
    List: Öğrenci listesini göster
    Write: Öğrenci listesini dosyaya kaydet
    Exit: Programdan çıkış yap
        """)
        choice = str(input('İşlemi seçiniz: '))

        if choice.lower() == 'exit':
            print('Hoççakaın ben ğidiyorum')
            break
        elif choice.lower() == 'new':
            student_manager.add_student()
        elif choice.lower() == 'list':
            student_manager.print_students()
        elif choice.lower() == 'write':
            student_manager.write_to_file()
        elif student_manager.check_student_exists(choice):
            student_manager.edit_students(choice)
        else:
            print('Lütfen bir komut ya da öğrenci numarası giriniz!')


if __name__ == '__main__':
    main()