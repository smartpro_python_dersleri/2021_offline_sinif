* "Sen Abdülhamidi savunmuşsun! Hayır savunmadım! Savundun! Çıkar Göster!" örneğini kodunuzun en başında bir değişken olarak atayın
* Sonrasında sırasıyla bu metindeki bütün sesli harfleri ayrı ayrı sayın
* Beraberinde "!" işaretlerini de sayın
* En son çıktıda "a = 5" şeklinde bütün sesli harfler ve ünlemlerin sayımlarını yazdırın

a = 8
e = 6