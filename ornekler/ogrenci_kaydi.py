from json import loads, dumps

student_list = {}

def read_file():
    """
    Proje klasörü içerisinde "student_list.json dosyasını okur.
    Eğer dosyayı bulamazsa boş bir json olarak yaratır.
    Eğer dosya içeriği json değilse de hata verdirtip programı kapatır.
    """
    try:
        with open('student_list.json') as f:
            new_dict = loads(f.read())
            student_list.update(new_dict)
    except FileNotFoundError:
        with open('student_list.json', 'w') as f:
            f.write(dumps({}))
    except ValueError as e:
        print('Dosya formati hatali')
        print(type(e))
        exit()
    finally:
        print('burasi da her turlu yazilacak')

def write_to_file(dict_to_write):
    json = dumps(dict_to_write)

    with open('student_list.json', 'w') as f:
        f.write(json)

def print_students():
    for k, v in student_list.items():
        print(k, v)

def create_student():
    user_id = None
    while True:
        user_id = str(input('Yeni öğrencinin numarası: '))
        if user_id in student_list or not user_id:
            print('Bu öğrenci numarası sistemde hali hazırda mevcuttur. Lütfen başka bir numara girin!')
        else:
            break
    fields = ['name', 'surname', 'age', 'gender', 'class', 'gpa']
    new_student = {}

    for i in fields:
        value = input(f'{i} alanı için değer girin: ')
        new_student.update({i: value})
    student_list.update({user_id: new_student})

    write_to_file(student_list)

def edit_student(student_id):
    student = student_list[student_id]
    field_choices = ['name', 'surname', 'age', 'gender', 'class', 'gpa']
    while True:
        print(student)
        field = None
        while True:
            field = input('Lütfen düzenleyeceğiniz alanı seçin: ')
            if field in field_choices:
                break
            else:
                print('Lütfen geçerli bir alan seçiniz')
        new_value = input(f'{field} alani için yeni bir değer giriniz: ')
        student.update({field: new_value})
        student_list.update({student_id: student})
        
        is_exit = input('Başka bir alan düzenlemek istemiyorsanız "Tamam" yazın: ')
        if is_exit.lower() == 'tamam':
            break

    write_to_file(student_list)

while True:
    read_file()
    print("""
    Lütfen yapacağınız işlemi seçiniz.
    Öğrenci no: Bir öğrenciyi düzenle
    New: Yeni bir öğrenci oluştur
    List: Öğrenci listesini göster
    Write: Öğrenci listesini dosyaya kaydet
    Exit: Programdan çıkış yap
    """)
    choice = str(input('İşlemi seçiniz: '))

    if choice.lower() == 'exit':
        print('Hoççakaın ben ğidiyorum')
        break
    elif choice.lower() == 'new':
        create_student()
    elif choice.lower() == 'list':
        print_students()
    elif choice.lower() == 'write':
        write_to_file(student_list)
    elif choice in student_list:
        edit_student(choice)
    else:
        print('Lütfen bir komut ya da öğrenci numarası giriniz!')