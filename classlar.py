# Classlar oluşturacağımız nesnenin kalıbıdır. Burada barındıracağı özellikleri belirleriz
# https://docs.python.org/3/tutorial/classes.html


class Deneme:
    name = None
    surname = None
    job = None

    def get_full_name(self):
        return f'{self.name} {self.surname}'
    
    def get_job_x(self, repeat):
        return self.job * repeat
    
    @staticmethod
    def get_two():
        return 2
        

# Nesnemizi oluşturmak için belirlediğimiz kalıbı bu şekilde çalıştırırız
mt = Deneme()
mt.name = 'Mahmut'
mt.surname = 'Tuncer'
mt.job = 'Lo'

#print(Deneme.get_full_name(mt))
print(mt.get_full_name())  # Mahmut Tuncer

#print(mt.get_job_x(3))
#print(mt.get_two())

class YeniDeneme(Deneme):
    age = None

    def get_full_name(self):
        return f'{self.surname} {self.name}'


it = YeniDeneme()
it.name = 'Ibrahim'
it.surname = 'Tatlises'
it.job = 'Mermiye Kafa Atma'
it.age = 'sonsuzluk'

class IkinciEvebeyn:
    gender = None

class CokEvebeynliClass(Deneme, IkinciEvebeyn):
    pass

yt = CokEvebeynliClass()
yt.name = 'Yildiz'
yt.surname = 'Tilbe'
yt.job = 'Overlokcu'
yt.age = 'kalbimizin yildizi'
yt.gender = 'Female'

print(yt.get_full_name(), yt.gender)

#print(it)
#print(it.get_full_name())
#print(it.job)
#print(it.age)

class Ogrenci:
    def __init__(self, name, surname, age, gender, classroom, gpa):
        self.name = name
        self.surname = surname
        self.age = age
        self.gender = gender
        self.classroom = classroom
        self.gpa = gpa
    
    def get_full_name(self):
        return f'{self.name} {self.surname}'

    @property
    def full_name(self):
        return self.get_full_name()

    def change_gpa(self, new_gpa):
        self.gpa = new_gpa

mk = Ogrenci(
    name='Musa',
    surname='Karaaslan',
    age=21,
    gender='Elephant',
    classroom='C305',
    gpa=79
)

print(mk.full_name)
print(mk.gpa)
mk.change_gpa(59)
print(mk.gpa)


class Insan:
    name = None
    surname = None
    age = None


class Ogrenci(Insan):
    pass


