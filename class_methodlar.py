class Hayvan:
    def __init__(self, **kwargs):
        self.name = kwargs['name']
        self.gender = kwargs['gender']
        self.species = kwargs['species']

    def __str__(self):
        return self.name
    
    def get_species(self):
        print(self.__class__)
        return self.species

    @classmethod
    def get_species_list(cls):
        print(cls)
        return ['kedi', 'köpek', 'kuş', 'at', 'timsah']

    @staticmethod
    def random_method(name):
        for i in name:
            print(i)

    
print(Hayvan.get_species_list())

pacchi = Hayvan(name='Pacchi', gender='Female', species='kedi')
print(pacchi.get_species_list())


class Kedi(Hayvan):
    def __init__(self, **kwargs):
        kwargs['species'] = 'kedi'
        super().__init__(**kwargs)
        self.colors = kwargs['colors']

    @classmethod
    def create_new_cat(cls):
        args = {'colors': []}
        for i in ['name', 'gender']:
            value = str(input(f'Please give a {i}: '))
            args.update({i: value})
        while True:
            value = str(input('Plase provide a color or press "q" to exit: '))
            if value.lower() == 'q':
                break
            args['colors'].append(value)
        return cls(**args)

class NorvecOrmanKedisi(Kedi):
    pass

candy = NorvecOrmanKedisi.create_new_cat()

print(type(candy))

pamuk = Kedi(
    name='Pamuk',
    gender='Male',
    colors=['white', 'yellow']
)

print(pamuk.__dict__)

ponpon = Kedi.create_new_cat()
print(ponpon.__dict__)