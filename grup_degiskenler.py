ornek_liste = ['ahmet', 'mehmet', 'huseyin', 'selvi', 'hikmet', 'mehmet', 'kubra','ali']

# 4. siradaki elemanı listeden alma
# print(ornek_liste[3])

# print(ornek_liste.pop(2))
# print(ornek_liste)

# ornek_liste.remove('hikmet')
# print(ornek_liste)

# print(ornek_liste.count('mehmet'))

# for i, v in enumerate(ornek_liste):
#    print(i, v)

## Set
ornek_set = {"elma", "armut", "ananas", "nar", 'elma', "avokado"}

# print(ornek_set)

# for i in ornek_set:
#    print(i)

## Tuple kullanimlari

abzurt_tanimlama = ('kedi', 'kopek', 'ayi', 'ornitoreng')
abzurt_tanimlama = 'kedi', 'kopek', 'ayi', 'ornitoreng'
# print(abzurt_tanimlama)

abzurt1, abzurt2, abzurt3, abzurt4 = abzurt_tanimlama
# print(abzurt1, abzurt4)

## sozluklerde birkac not

ornek_sozluk = {"name": "Kubra", "age": 22, "class": "C305"}
ornek_sozluk = dict(name="Kubra", age=22, clas="C305")

for k, v in ornek_sozluk.items():
    print(k, v)

# print(list('Merhaba Dunya'))