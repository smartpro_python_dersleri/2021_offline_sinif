class Hayvan:
    name = None
    age = None
    gender = None

    def __str__(self):
        return self.name

    def get_dict(self):
        return self.__dict__

    def __add__(self, other):
        return self.age + other.age


class Kedi(Hayvan):
    color = None
    number_of_trafos = 0

    def add_trafo(self):
        self.number_of_trafos += 1


class Kopek(Hayvan):
    color = None
    breed = None


class SivasKangal(Kopek):
    height = None
    breed = 'Sivas Kangal'


class Insan(Hayvan):
    job = None


class Pet:
    owner: Insan
    animal = None

    def __str__(self):
        return f'{self.owner} kisisinin evcil hayvani {self.animal}'


musa = Insan()
musa.name = 'Musa Karaaslan'
musa.age = 21
musa.gender = '?'
musa.job = None

bozo = Kopek()
bozo.name = 'Bozo'
bozo.age = 1
bozo.breed = 'Morkie'
bozo.color = 'Beyaz'

print(musa + bozo)

musa_bozo = Pet()
musa_bozo.owner = musa
musa_bozo.animal = bozo

print(musa_bozo)
print(musa_bozo.owner)
print(musa_bozo.animal)
print(musa_bozo.owner.get_dict())

del musa_bozo

# print(musa_bozo)