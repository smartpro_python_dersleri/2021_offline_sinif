def benim_bir_fonksiyonum_var():
    print('Cok guzel bir fonksiyon hem de')

benim_bir_fonksiyonum_var()

def sordum_sari_cicege(soru):
    print(soru)

sordum_sari_cicege('annen, baban, var midir?')

def doktor_bu_ne(nesne='Doritos'):
    def make_upper(nesne):
        return nesne.upper()
    print(make_upper(nesne))

doktor_bu_ne()
doktor_bu_ne(nesne='Lays')

def ne_vereyim_abime():
    return 'Little_little_into_the_middle'

sofra = ne_vereyim_abime()

def make_upper(string):
    return string.upper()

print(
    make_upper(
        ne_vereyim_abime()
    )        
)