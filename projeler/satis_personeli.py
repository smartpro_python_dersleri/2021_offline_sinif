from dataclasses import dataclass, field


mock_database = [
    {
        'name': 'Mahmut',
        'surname': 'Tuncer',
        'commision_rate': 5,
        'sales': [
            {
                'customer': 'Flash TV',
                'price': 69.90,
                'date': '01/01/1970',
            },
            {
                'customer': 'Flash TV',
                'price': 420.80,
                'date': '01/02/2003'
            },
        ]
    },
    {
        'name': 'Davut',
        'surname': 'Güloğlu',
        'commision_rate': 12,
        'sales': [
            {
                'customer': 'Elon Musk',
                'price': 66898.99,
                'date': '20/03/2021',
            },
            {
                'customer': 'E.T.',
                'price': 500,
                'date': '04/06/1982'
            },
        ]
    },
]


@dataclass
class Sale:
    customer: str
    date: str
    price: float
    commision_rate: float

    def __str__(self):
        return f'{self.customer} - {self.price}'

    @property
    def commision(self):
        return self.calculate_commision()

    def calculate_commision(self):
        return self.price * self.commision_rate / 100


@dataclass
class Personel:
    name: str
    surname: str
    sales: list = field(default_factory=list)
    commision_rate: float = 10

    def __str__(self):
        return f'{self.name} {self.surname}'

    def create_sale(self, customer, date, price):
        new_sale = Sale(
            customer=customer,
            date=date,
            price=price,
            commision_rate=self.commision_rate
        )
        self.sales.append(new_sale)

    def total_commisions(self):
        return sum(
            [x.commision for x in self.sales]
        )

    def print_all_sales(self):
        for i in self.sales:
            print(
                i.customer, i.price, i.date, i.commision, sep=' | '
            )


class CompanyManager:
    personels = []

    def __init__(self):
        self._get_personels_from_db(mock_database)

    def add_personel(self, name, surname, commision_rate=10):
        personel = Personel(
            name=name,
            surname=surname,
            commision_rate=commision_rate,
        )
        self.personels.append(personel)

    def print_personels(self):
        for i, v in enumerate(self.personels):
            print(f'{i} - {v}')

    def print_all_sales(self):
        for personel in self.personels:
            print(personel)
            personel.print_all_sales()

    def _get_personel_from_dict(self, data):
        personel = Personel(
            name=data['name'],
            surname=data['surname'],
            commision_rate=data['commision_rate']
        )

        for i in data['sales']:
            personel.create_sale(
                customer=i['customer'],
                date=i['date'],
                price=i['price']
            )
        return personel

    def _get_personels_from_db(self, db_list):
        for i in db_list:
            self.personels.append(self._get_personel_from_dict(i))


def main():
    company_manager = CompanyManager()

    company_manager.print_all_sales()

    company_manager.print_personels()

    for i in company_manager.personels:
        print(f'{i} - {i.total_commisions()}')


if __name__ == '__main__':
    main()
