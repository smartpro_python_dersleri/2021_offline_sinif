Bu projede satış elemanlarının ve yaptıkları satışların kayıtları tutulacak. Bu uygulamada sizden istenilen şeyler:
- Satış personelinin adı, soyadı ve personele özel komisyon oranı tutulacak.
- Komisyon oranı öntanımlı olarak %10 olacak. Ancak isteğe bağlı değişebilecek.
- Satış personelinin yaptığı satışlar listelenebilecek.
- Satışlar müşteri firma, satış tarihi ve satış ücretini barındıracak.
- Satışlarda personelin komisyonu sehaplanabilecek.
- Personelin toplam komisyonu hesaplanabilecek.
- Firmanın bütün satışları listelenebilecek.