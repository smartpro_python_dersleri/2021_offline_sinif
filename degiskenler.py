deneme = "bu bir deneme degiskenidir"
sayi = 12
kesirli_sayi = 24.6
boolean = True
bos = None

liste = ['ahmet', 'mehmet', 'veli', 42, False]

liste.append('husamettin')

tuple_degisken = ('ahmet', 'mehmet', 'veli')

sozluk = {
    'ad': 'Ali',
    'yas': 42,
    'boy': 1.50
    }

sozluk['ad']
sozluk.get('yas')

sozluk['ad'] = 'Hayriye'
sozluk.update({'soyad': 'Firinci'})
sozluk['yas'] = 39
sozluk.update({'boy': 1.49})

ikinci_sozluk = {
    'kilo': 150,
    'meslek': 'e-spor',
}

sozluk.update(ikinci_sozluk)